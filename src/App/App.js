import React from 'react';
import './App.scss';

function App() {
  return (
    <div className="App">
      <div className="button-section">
        <h1>Button Section</h1>
        <button className="default-button">Default Button</button>
        <button className="custom-button">Custom Button</button>
      </div>
      <div className="EUR">
        <span class="price">45</span>
      </div>
      
    </div>
  );
}

export default App;
